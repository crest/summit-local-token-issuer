FROM docker-registry.summit-qa.research.vt.edu/vt-devcom/token-issuer-app:commit-e0b7a436e2994ee71e5b8061fa0159739f0a4dfd

ENV OAUTH_LOCAL_KEY_STORAGE_PASSWORD super-secret-password
ENV OAUTH_KEY_PAIR_ID default
ENV OAUTH_LOCAL_KEY_PAIR_STORAGE_DIRECTORY /etc/token-issuer/keys
ENV OAUTH_LOCAL_KEY_PAIR_PASSWORD_FILE /etc/token-issuer/key-password
ENV OAUTH_ACCESS_TOKEN_LIFETIME 2
ENV OAUTH_ACCESS_TOKEN_LIFETIME_UNITS minutes
ENV OAUTH_REFRESH_TOKEN_LIFETIME 10
ENV OAUTH_REFRESH_TOKEN_LIFETIME_UNITS minutes

ENV OAUTH_REGISTRY_URI file:/registry.json
COPY registry.json /registry.json

COPY config.d /etc/wildfly/config.d/
COPY *.properties /etc/token-issuer-app/
